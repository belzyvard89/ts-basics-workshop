{
    // basics
    let myName = "dario";
    let myAge = 32;
    let isItalian = true;

    console.log(myAge);

    // interface and type
    // object containing properties -> interface
    // type aliasing or similar -> type


    type MyId = string | number;
    type Platform = "web" | "ios" | "android";

    // not suggested
    type Hobby = {
        name: string,
        isTimeConsuming: boolean
    }

    interface PersonData {
        id: MyId,
        name: string,
        middleName?: string,
        lastname: string,
        age: number,

        // other possible options...
        giveMeAJoke: (isPun: boolean) => string,
        favColors?: string[],

        hobby?: Hobby,
    }

    let darioData = {
        id: 1,
        name: "dario",
        lastname: "randazzo",
        middleName: "phil",
        age: 32,
        giveMeAJoke: (isJoke: boolean) => { return "What do you call sad coffee? A: Despresso" }
        // middleName: "phil"
    }

    interface DevData extends PersonData {
        platform: Platform,
    }

    let darioDevData: DevData = {
        // name: "dario",
        // lastname: "randazzo",
        // age: 32,
        ...darioData,
        platform: "web"
    }

    const printPersonName = (person: PersonData) => {
        console.log("name is ", person.name);
    }

    const printHobby = (person: PersonData) => {
        
        console.log("fullname is ", person.hobby?.name)
    }

    // thanks to Inference & Structural Type System we can pass an object
    // that just has the "shape" of PersonData, so it's not mandatory its declaration
    printHobby(darioData);

    const printId = (person: PersonData) : string  => {
        if(typeof person.id === "string"){
            return person.id;
        }
        return person.id.toString();
    }


}