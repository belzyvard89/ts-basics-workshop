var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
{
    // basics
    var myName = "dario";
    var myAge = 32;
    var isItalian = true;
    console.log(myAge);
    var darioData = {
        id: 1,
        name: "dario",
        lastname: "randazzo",
        middleName: "phil",
        age: 32,
        giveMeAJoke: function (isJoke) { return "What do you call sad coffee? A: Despresso"; }
        // middleName: "phil"
    };
    var darioDevData = __assign(__assign({}, darioData), { platform: "web" });
    var printPersonName = function (person) {
        console.log("name is ", person.name);
    };
    var printHobby = function (person) {
        var _a;
        console.log("fullname is ", (_a = person.hobby) === null || _a === void 0 ? void 0 : _a.name);
    };
    // thanks to Inference & Structural Type System we can pass an object
    // that just has the "shape" of PersonData, so it's not mandatory its declaration
    printHobby(darioData);
    var printId = function (person) {
        if (typeof person.id === "string") {
            return person.id;
        }
        return person.id.toString();
    };
}
